package com.example.example.service;

import com.example.example.exception.ServiceException;
import com.example.example.model.Model;

/**
 * Created by pete on 05/02/2014.
 */
public interface ModelService {
    void createThenUpdateModel(Model model) throws ServiceException;
}
