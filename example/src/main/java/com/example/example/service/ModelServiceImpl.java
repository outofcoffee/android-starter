package com.example.example.service;

import com.example.example.data.DbHelper;
import com.example.example.data.DbHelperFactory;
import com.example.example.exception.ServiceException;
import com.example.example.model.Model;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.misc.TransactionManager;

import java.sql.SQLException;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import javax.inject.Inject;

/**
 * Created by pete on 05/02/2014.
 */
public class ModelServiceImpl implements ModelService {

    private static final Logger LOGGER = Logger.getLogger(ModelServiceImpl.class.getSimpleName());

    @Inject
    private DbHelperFactory dbHelperFactory;

    @Override
    public void createThenUpdateModel(final Model model) throws ServiceException {
        final DbHelper helper = dbHelperFactory.get();
        try {
            final RuntimeExceptionDao<Model, String> dao = helper.getAlbumDao();

            // perform operations in a transaction
            TransactionManager.callInTransaction(helper.getConnectionSource(),
                    new Callable<Void>() {
                        public Void call() throws Exception {
                            // create it
                            dao.create(model);
                            LOGGER.info("Created model: " + model);

                            // update it
                            model.setTitle("Different title");

                            dao.update(model);
                            LOGGER.info("Updated model: " + model);

                            return null;
                        }
                    });

        } catch (SQLException e) {
            throw new ServiceException(String.format("Error creating or updating model %s", model), e);

        } finally {
            helper.close();
        }
    }
}