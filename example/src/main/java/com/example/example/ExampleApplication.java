/**
 *  Copyright Pete Cornish
 *
 *  Unless required by applicable law or agreed to in writing, this
 *  software is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
package com.example.example;

import android.app.Application;

import com.testflightapp.lib.TestFlight;

import roboguice.RoboGuice;

/**
 * Application.
 *
 * @author pete
 */
public class ExampleApplication extends Application {

    /**
     *
     */
    @Override
    public void onCreate() {
        super.onCreate();

//        // set up TF
//        TestFlight.takeOff(this, "TF token here");

        // set up DI
        RoboGuice.setBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE, RoboGuice.newDefaultRoboModule(this),
                new DependencyModule());
    }
}
