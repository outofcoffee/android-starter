package com.example.example.ui;

import android.os.Bundle;

import com.example.example.R;
import com.example.example.exception.ServiceException;
import com.example.example.model.Model;
import com.example.example.service.ModelService;
import com.google.inject.Inject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by pete on 05/02/2014.
 */
public class MainActivity extends RoboActionBarActivity {

    private static final Logger LOGGER = Logger.getLogger(MainActivity.class.getSimpleName());

    @Inject
    private ModelService modelService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final Model model = new Model();
        model.setTitle("Example");

        try {
            modelService.createThenUpdateModel(model);
        } catch (ServiceException e) {
            LOGGER.log(Level.SEVERE, "Error saving model object", e);
        }
    }
}
