/**
 *  Copyright Pete Cornish
 *
 *  Unless required by applicable law or agreed to in writing, this
 *  software is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
package com.example.example;

import com.example.example.data.DbHelperFactory;
import com.example.example.data.DbHelperFactoryImpl;
import com.example.example.service.ModelService;
import com.example.example.service.ModelServiceImpl;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

/**
 * Guice configuration.
 *
 * @author pete
 */
public class DependencyModule implements Module {

    /**
     * {@inheritdoc}
     */
    @Override
    public void configure(Binder binder) {
        // daos
        binder.bind(DbHelperFactory.class).to(DbHelperFactoryImpl.class).in(Singleton.class);

        // services
        binder.bind(ModelService.class).to(ModelServiceImpl.class).in(Singleton.class);
    }

}
