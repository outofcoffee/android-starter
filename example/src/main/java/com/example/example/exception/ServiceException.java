package com.example.example.exception;

import java.sql.SQLException;

/**
 * Created by pete on 05/02/2014.
 */
public class ServiceException extends Throwable {
    public ServiceException(String message, Exception e) {
        super(message, e);
    }
}
