/**
 *  Copyright Pete Cornish
 *
 *  Unless required by applicable law or agreed to in writing, this
 *  software is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
package com.example.example.data;

/**
 * Provide instances of {@link DbHelper}.
 * 
 * @author pete
 */
public interface DbHelperFactory {

    /**
     * Get an instance of {@link DbHelper}.
     * 
     * @return
     */
    DbHelper get();

}
