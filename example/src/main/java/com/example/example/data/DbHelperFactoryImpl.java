/**
 *  Copyright Pete Cornish
 *
 *  Unless required by applicable law or agreed to in writing, this
 *  software is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 */
package com.example.example.data;

import android.content.Context;

import com.google.inject.Inject;

import java.util.logging.Logger;

/**
 * Provide singleton instance of {@link DbHelper}. It's still the responsibility of the caller to call
 * {@link DbHelper#close()}.
 * 
 * @author pete
 */
public class DbHelperFactoryImpl implements DbHelperFactory {

    private static final Logger LOGGER = Logger.getLogger(DbHelperFactoryImpl.class.getSimpleName());

    @Inject
    private Context context;

    /**
     * Singleton.
     */
    private volatile DbHelper helper;

    /**
     * Users of {@link DbHelper}.
     */
    private volatile int useCount = 0;

    /**
     * {@inheritdoc}
     */
    @Override
    public synchronized DbHelper get() {
        if (null == helper) {
            LOGGER.fine("Creating new DB helper");
            helper = new DbHelper(context) {

                /**
                 * Only close if useCount == 0;
                 */
                @Override
                public void close() {
                    if (--useCount <= 0) {
                        LOGGER.fine("Closing DB helper");
                        super.close();

                        // de-reference self
                        helper = null;
                    }
                }
            };
        }

        useCount++;
        return helper;
    }

}
