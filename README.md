# Sample Android starter project with DI, ORM, TestFlight and support libraries.

## Includes
- Guice dependency injection
- ORMLite for ORM
- TestFlight SDK
- Android v4 and v7 support libraries

## Configuration
- Compatible with API 8 and above
- Uses Gradle Android plugin
